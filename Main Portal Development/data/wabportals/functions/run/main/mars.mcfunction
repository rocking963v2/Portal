# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

                 # This funtion checks mars portal stuff! #

# ----------------------------------------------------------------------------- #

# If there's an entity in the portal, load the other dimension to check if there's a portal
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,tag=wabportals_mars_marker,sort=nearest,limit=1] in minecraft:mars run forceload add ~5 ~5 ~-5 ~-5
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/mars] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,tag=wabportals_mars_marker,sort=nearest,limit=1] in minecraft:overworld run forceload add ~5 ~5 ~-5 ~-5

# If there is a portal in the other dimension, tp the player
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_mars_marker] at @s in minecraft:mars if entity @e[type=marker,tag=wabportals_mars_marker,predicate=wabportals:dimensions/mars,distance=..384] in minecraft:overworld as @e[predicate=wabportals:travellers/all_mobs,predicate=wabportals:dimensions/overworld,distance=...5,scores={wabportals_cooldown=0}] at @s run function wabportals:run/teleport/mars/dim1
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/mars] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_mars_marker] at @s in minecraft:overworld if entity @e[type=marker,tag=wabportals_mars_marker,predicate=wabportals:dimensions/overworld,distance=..384] in minecraft:mars as @e[predicate=wabportals:travellers/all_mobs,predicate=wabportals:dimensions/mars,distance=...5,scores={wabportals_cooldown=0}] at @s run function wabportals:run/teleport/mars/dim2

# Make a portal if there isn't one
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_mars_marker] at @s in minecraft:mars unless entity @e[type=marker,tag=wabportals_mars_marker,predicate=wabportals:dimensions/mars,distance=..384] align xyz run function wabportals:run/create_alternate/mars/dim1
execute as @e[type=armor_stand,tag=wabportals_mars_portal_stand,predicate=wabportals:dimensions/mars] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_mars_marker] at @s in minecraft:overworld unless entity @e[type=marker,tag=wabportals_mars_marker,predicate=wabportals:dimensions/overworld,distance=..384] align xyz run function wabportals:run/create_alternate/mars/dim2

# Check if any portals got broken, and if so, close them.
execute as @e[type=marker,tag=wabportals_mars_marker_z] at @s run function wabportals:run/check_if_broken/mars/z
execute as @e[type=marker,tag=wabportals_mars_marker_x] at @s run function wabportals:run/check_if_broken/mars/x


