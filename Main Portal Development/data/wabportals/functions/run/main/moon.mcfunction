# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

                 # This funtion checks moon portal stuff! #

# ----------------------------------------------------------------------------- #

# If there's an entity in the portal, load the other dimension to check if there's a portal
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,tag=wabportals_moon_marker,sort=nearest,limit=1] in minecraft:moon run forceload add ~5 ~5 ~-5 ~-5
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/moon] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,tag=wabportals_moon_marker,sort=nearest,limit=1] in minecraft:overworld run forceload add ~5 ~5 ~-5 ~-5

# If there is a portal in the other dimension, tp the player
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_moon_marker] at @s in minecraft:moon if entity @e[type=marker,tag=wabportals_moon_marker,predicate=wabportals:dimensions/moon,distance=..384] in minecraft:overworld as @e[predicate=wabportals:travellers/all_mobs,predicate=wabportals:dimensions/overworld,distance=...5,scores={wabportals_cooldown=0}] at @s run function wabportals:run/teleport/moon/dim1
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/moon] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_moon_marker] at @s in minecraft:overworld if entity @e[type=marker,tag=wabportals_moon_marker,predicate=wabportals:dimensions/overworld,distance=..384] in minecraft:moon as @e[predicate=wabportals:travellers/all_mobs,predicate=wabportals:dimensions/moon,distance=...5,scores={wabportals_cooldown=0}] at @s run function wabportals:run/teleport/moon/dim2

# Make a portal if there isn't one
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/overworld] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_moon_marker] at @s in minecraft:moon unless entity @e[type=marker,tag=wabportals_moon_marker,predicate=wabportals:dimensions/moon,distance=..384] align xyz run function wabportals:run/create_alternate/moon/dim1
execute as @e[type=armor_stand,tag=wabportals_moon_portal_stand,predicate=wabportals:dimensions/moon] at @s if entity @e[predicate=wabportals:travellers/all_mobs,distance=..0.5,scores={wabportals_cooldown=0}] as @e[type=marker,sort=nearest,limit=1,tag=wabportals_moon_marker] at @s in minecraft:overworld unless entity @e[type=marker,tag=wabportals_moon_marker,predicate=wabportals:dimensions/overworld,distance=..384] align xyz run function wabportals:run/create_alternate/moon/dim2

# Check if any portals got broken, and if so, close them.
execute as @e[type=marker,tag=wabportals_moon_marker_z] at @s run function wabportals:run/check_if_broken/moon/z
execute as @e[type=marker,tag=wabportals_moon_marker_x] at @s run function wabportals:run/check_if_broken/moon/x


