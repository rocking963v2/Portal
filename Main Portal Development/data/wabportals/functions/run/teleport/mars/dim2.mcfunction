# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

                    # This function teleports the player! #

# ----------------------------------------------------------------------------- #

# Check if the marker has landed
execute in minecraft:overworld at @e[type=marker,predicate=wabportals:dimensions/overworld,tag=wabportals_mars_marker,sort=nearest,limit=1] run tp @s ~ ~ ~
scoreboard players set @s wabportals_cooldown 300
execute in minecraft:mars run forceload remove ~5 ~5 ~-5 ~-5
execute in minecraft:overworld run forceload remove ~5 ~5 ~-5 ~-5