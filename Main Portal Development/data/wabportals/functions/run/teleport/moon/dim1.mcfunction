# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

                    # This function teleports the player! #

# ----------------------------------------------------------------------------- #

# Check if the marker has landed
execute in minecraft:moon at @e[type=marker,predicate=wabportals:dimensions/moon,tag=wabportals_moon_marker,sort=nearest,limit=1] run tp @s ~ ~ ~
scoreboard players set @s wabportals_cooldown 300
execute in minecraft:moon run forceload remove ~5 ~5 ~-5 ~-5
execute in minecraft:overworld run forceload remove ~5 ~5 ~-5 ~-5