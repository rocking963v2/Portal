# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

            # This function runs every time the world is loaded! #

# ----------------------------------------------------------------------------- #

# Check if the player used what's needed to activate the portal

scoreboard objectives add moon_portal_activation_check minecraft.used:water_bucket 
scoreboard objectives add mars_portal_activation_check_flint minecraft.used:flint_and_steel 
scoreboard objectives add mars_portal_activation_check_charge minecraft.used:fire_charge 

# Check if raycasts have exceeded the player's reach range yet

scoreboard objectives add player_reach_range dummy

# Make a scoreboard for the timer function

scoreboard objectives add wabportals_timer dummy

# Make a scoreboard to store an entity's coordinates

scoreboard objectives add wabportals_cooldown dummy