# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

            # This function checks where the portal was activated! #

# ----------------------------------------------------------------------------- #

# Check where the block the player placed is

execute if block ~ ~ ~ #wabportals:activation/moon if block ~ ~-1 ~ #wabportals:frame/moon run function wabportals:activate/check_frame/moon/bottom
execute if block ~ ~ ~ #wabportals:activation/moon if block ~ ~-2 ~ #wabportals:frame/moon run function wabportals:activate/check_frame/moon/mid
execute if block ~ ~ ~ #wabportals:activation/moon if block ~ ~-3 ~ #wabportals:frame/moon run function wabportals:activate/check_frame/moon/top
scoreboard players remove @s player_reach_range 1
execute if score @s player_reach_range matches 1.. if block ~ ~ ~ #wabportals:air positioned ^ ^ ^0.5 run function wabportals:activate/locate_block/moon