# ----------------------------------------------------------------------------- #

                    # Custom Dimension Portals Data Pack #
                        # Made by WafflesAreBetter #
                # https://www.youtube.com/c/WafflesAreBetter #

             # This funtion runs every tick! (20 times a second) #

# ----------------------------------------------------------------------------- #

# Check if the player used what's needed to activate the portal

scoreboard players set @a player_reach_range 13


execute as @a[scores={moon_portal_activation_check=1..},predicate=wabportals:dimensions/overworld] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/moon
execute as @a[scores={moon_portal_activation_check=1..},predicate=wabportals:dimensions/moon] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/moon

execute as @a[scores={mars_portal_activation_check_flint=1..},predicate=wabportals:dimensions/overworld] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/mars
execute as @a[scores={mars_portal_activation_check_flint=1..},predicate=wabportals:dimensions/mars] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/mars

execute as @a[scores={mars_portal_activation_check_charge=1..},predicate=wabportals:dimensions/overworld] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/mars
execute as @a[scores={mars_portal_activation_check_charge=1..},predicate=wabportals:dimensions/mars] at @s rotated as @s anchored eyes run function wabportals:activate/locate_block/mars

scoreboard players set @a moon_portal_activation_check 0
scoreboard players set @a mars_portal_activation_check_flint 0
scoreboard players set @a mars_portal_activation_check_charge 0

execute as @e[type=minecraft:armor_stand,tag=wabportals_moon_portal_stand] at @s run particle minecraft:squid_ink ~ ~1 ~ 0.5 0.5 0.1 0 1
execute as @e[type=minecraft:armor_stand,tag=wabportals_mars_portal_stand] at @s run particle minecraft:flame ~ ~1 ~ 0.5 0.5 0.1 0 1

function wabportals:run/main/moon
function wabportals:run/main/mars

execute as @e[predicate=wabportals:travellers/all_mobs] unless score @s wabportals_cooldown = @s wabportals_cooldown run scoreboard players set @s wabportals_cooldown 0
execute as @e[predicate=wabportals:travellers/all_mobs,scores={wabportals_cooldown=1..}] unless entity @e[type=armor_stand,tag=wabportals_portal_stand,distance=...5] run scoreboard players remove @s wabportals_cooldown 1
